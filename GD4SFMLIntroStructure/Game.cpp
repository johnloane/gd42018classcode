#include "Game.hpp"
#include "constants.hpp"
#include "StringHelpers.hpp"

Game::Game():mWindow(sf::VideoMode(640, 480), "SFML Test Better Structure"), mPlayer(), mFont(), mStatisticsText(), mStatisticsUpdateTime(), mStatisticsNumFrames(0), mIsMovingUp(false), mIsMovingDown(false), mIsMovingLeft(false), mIsMovingRight(false), mTexture()
{
	if (!mTexture.loadFromFile("Media/Textures/Eagle.png"))
	{
		//Handle error 
	}
	mPlayer.setTexture(mTexture);
	mPlayer.setPosition(100.f, 100.f);

	mFont.loadFromFile("Media/Sansation.ttf");
	mStatisticsText.setFont(mFont);
	mStatisticsText.setPosition(5.f, 5.f);
	mStatisticsText.setCharacterSize(20);
	
}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (mWindow.isOpen())
	{
		sf::Time elapsedTime = clock.restart();
		timeSinceLastUpdate += elapsedTime;
		while (timeSinceLastUpdate > TIMEPERFRAME)
		{
			timeSinceLastUpdate -= TIMEPERFRAME;
			processEvents();
			update(TIMEPERFRAME);
		}
		updateStatistics(elapsedTime);
		render();
	}
}

void Game::processEvents()
{
	sf::Event event;
	while (mWindow.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyPressed:
			handlePlayerInput(event.key.code, true);
			break;
		case sf::Event::KeyReleased:
			handlePlayerInput(event.key.code, false);
			break;
		case sf::Event::Closed:
			mWindow.close();
			break;
		}
	}
}

void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if (key == sf::Keyboard::W)
	{
		mIsMovingUp = isPressed;
	}
	else if (key == sf::Keyboard::S)
	{
		mIsMovingDown = isPressed;
	}
	if (key == sf::Keyboard::A)
	{
		mIsMovingLeft = isPressed;
	}
	if (key == sf::Keyboard::D)
	{
		mIsMovingRight = isPressed;
	}
}

void Game::update(sf::Time dt)
{
	sf::Vector2f movement(0.f, 0.f);
	if (mIsMovingUp)
	{
		movement.y -= PLAYERSPEED;
	}
	if (mIsMovingDown)
	{
		movement.y += PLAYERSPEED;
	}
	if (mIsMovingLeft)
	{
		movement.x -= PLAYERSPEED;
	}
	if (mIsMovingRight)
	{
		movement.x += PLAYERSPEED;
	}
	mPlayer.move(movement * dt.asSeconds());
}

void Game::render()
{
	mWindow.clear();
	mWindow.draw(mPlayer);
	mWindow.draw(mStatisticsText);
	mWindow.display();
}

void Game::updateStatistics(sf::Time elapsedTime)
{
	mStatisticsUpdateTime += elapsedTime;
	mStatisticsNumFrames += 1;
	if (mStatisticsUpdateTime >= sf::seconds(1.0f))
	{
		mStatisticsText.setString("Frames/Second = " + toString(mStatisticsNumFrames) + "\n" + "Time/Update = " + toString(mStatisticsUpdateTime.asMicroseconds() / mStatisticsNumFrames) + "us");
		mStatisticsUpdateTime -= sf::seconds(1.0f);
		mStatisticsNumFrames = 0;
	}
	
}
