#pragma once
#include <sstream>

//Since std::to_string doesn't work on MinGW we have to implement it ourselves
template <typename T>
std::string toString(const T& value);
#include "StringHelpers.inl"